package com.goole.androidexercise.prop_animation;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;

import com.goole.androidexercise.R;
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class PropAnimActivity extends AppCompatActivity {

    private final static String TAG = PropAnimActivity.class.getSimpleName();

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prop_anim_layout);
        mTextView = (TextView) findViewById(R.id.id_prop_tv);

        ValueAnimator animator = new ValueAnimator();
        animator.ofFloat(0, 100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

            }
        });

        ObjectAnimator objectAnimator = new ObjectAnimator();
        /* Sets the Outline of the View. */
        mTextView.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(view.getPaddingLeft(),
                        view.getPaddingTop(),
                        view.getWidth() - view.getPaddingRight(),
                        view.getHeight() - view.getPaddingBottom());
            }
        });

        /* Enables clipping on the View. */
        mTextView.setClipToOutline(true);
    }

    public void click(View view) {
        ViewPropertyAnimator animator = mTextView.animate();
        //animator.translationYBy(100).translationXBy(100).rotationBy(360);
        animator.rotationXBy(90);
        animator.setUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                Log.d(TAG, "value = " + value);
            }
        });
    }
}
