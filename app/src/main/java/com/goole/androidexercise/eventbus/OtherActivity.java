package com.goole.androidexercise.eventbus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.goole.androidexercise.R;

import org.greenrobot.eventbus.EventBus;

public class OtherActivity extends AppCompatActivity {

    private Button mSendMsgBtn;
    private Button mSendStickyMsgBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

        mSendMsgBtn = (Button) findViewById(R.id.btn_message);
        mSendStickyMsgBtn = (Button) findViewById(R.id.btn_sticky_message);

        mSendMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MessageEvent("Message from Other Activity"));
                finish();
            }
        });

        mSendStickyMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().postSticky(new MessageEvent("Receive Sticky Event"));
                finish();
            }
        });
    }
}
