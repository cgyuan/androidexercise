package com.goole.androidexercise.widget.cardview;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.goole.androidexercise.R;

/**
 *  使用这些属性定制 CardView 小组件的外观：
 *
 *  如果要在您的布局中设置圆角半径，请使用 card_view:cardCornerRadius 属性。
 *  如果要在您的代码中设置圆角半径，请使用 CardView.setRadius 方法。
 *  如果要设置卡片的背景颜色，请使用 card_view:cardBackgroundColor 属性。
 */
public class CardViewSampleActivity extends Activity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private String[] mDataSet = {"JAVA", "ANDROID", "PHP", "C++", "PYTHON"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recycler_view_layout);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CardViewAdapter(mDataSet);
        mRecyclerView.setAdapter(mAdapter);
    }
}

