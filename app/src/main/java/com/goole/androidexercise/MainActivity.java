package com.goole.androidexercise;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.CaptioningManager;
import android.widget.Toast;

import com.goole.androidexercise.animator.view.DownloadProgressBar;
import com.goole.androidexercise.swipelayout.view.SwipeLayout;

import java.lang.reflect.InvocationTargetException;

public class MainActivity extends AppCompatActivity {

    private DownloadProgressBar mBar;
    //private SwipeLayout mSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subtitle_layout);

        SubtitleView subtitleView = (SubtitleView) findViewById(R.id.subtitle_view);
        subtitleView.setText("There are 20+ new features in this release focused on helping you code faster and smarter. ");
        subtitleView.setTextSize(150);
        subtitleView.setBackgroundColor(Color.BLACK);
        subtitleView.setForegroundColor(Color.BLUE);
        subtitleView.setEdgeType(CaptioningManager.CaptionStyle.EDGE_TYPE_RAISED);
        subtitleView.setEdgeColor(Color.RED);
        subtitleView.setTypeface(Typeface.createFromAsset(getAssets(), "BrannbollFet.ttf"));


        //mBar = (DownloadProgressBar) findViewById(R.id.id_download_bar);
        /*mSwipeLayout = (SwipeLayout) findViewById(R.id.id_swipelayout);
        mSwipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSwipeLayout.isOpen()) {
                    mSwipeLayout.close();
                } else {
                    Toast.makeText(MainActivity.this, "click", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mSwipeLayout.getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewGroup)mSwipeLayout.getParent()).removeView(mSwipeLayout);
            }
        });*/
    }

    /*public void click(View v) {
        mBar.start();
    }*/
}
