package com.goole.androidexercise.provider;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.goole.androidexercise.R;

import java.io.File;

public class FileSelectActivity extends AppCompatActivity {

    private static final String TAG = FileSelectActivity.class.getSimpleName();

    // The path to the root of this app's internal storage
    private File mPrivateRootDir;
    // The path to the "images" subdirectory
    private File mImagesDir;
    // Array of files in the images subdirectory
    File[] mImageFiles;
    // Array of filenames corresponding to mImageFiles
    String[] mImageFilenames;

    Intent mResultIntent;

    private ListView mFileListView;
    private Uri fileUri;


    // Initialize the Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_select);

        mFileListView = (ListView) findViewById(R.id.list_view);

        // Set up an Intent to send back to apps that request a file
        mResultIntent =
                new Intent("com.goole.androidexercise.ACTION_RETURN_FILE");
        // Get the files/ subdirectory of internal storage
        mPrivateRootDir = getFilesDir();

        Log.d(TAG, "path  = " + mPrivateRootDir.getAbsolutePath());

        // Get the files/images subdirectory;
        mImagesDir = new File(mPrivateRootDir, "files");

        Log.d(TAG, "path  = " + mImagesDir.getAbsolutePath());

        // Get the files in the images subdirectory
        mImageFiles = mImagesDir.listFiles();

        Log.d(TAG, "path  = " + mImageFiles.toString());

        // Set the Activity's result to null to begin with
        setResult(Activity.RESULT_CANCELED, null);

        if(mImageFiles == null) return;
        mImageFilenames = new String[mImageFiles.length];
        for (int i = 0; i < mImageFiles.length; i++) {
            mImageFilenames[i] = mImageFiles[i].getName();
        }

        mFileListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mImageFilenames));

        mFileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*
                 * Get a File for the selected file name.
                 * Assume that the file names are in the
                 * mImageFilename array.
                 */
                File requestFile = mImageFiles[position];
                /*
                 * Most file-related method calls need to be in
                 * try-catch blocks.
                 */
                // Use the FileProvider to get a content URI
                try {
                    fileUri = FileProvider.getUriForFile(
                            FileSelectActivity.this,
                            "com.goole.androidexercise.fileprovider",
                            requestFile);
                    if (fileUri != null) {
                        // Grant temporary read permission to the content URI
                        mResultIntent.addFlags(
                                Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        // Put the Uri and MIME type in the result Intent
                        mResultIntent.setDataAndType(
                                fileUri,
                                getContentResolver().getType(fileUri));
                        // Set the result
                        FileSelectActivity.this.setResult(Activity.RESULT_OK,
                                mResultIntent);

                    } else {
                        mResultIntent.setDataAndType(null, "");
                        FileSelectActivity.this.setResult(RESULT_CANCELED,
                                    mResultIntent);
                    }

                } catch (IllegalArgumentException e) {
                    Log.e("File Selector",
                            "The selected file can't be shared: " +
                                    mImageFilenames[position]);
                }
                finish();
            }
        });
    }
}
