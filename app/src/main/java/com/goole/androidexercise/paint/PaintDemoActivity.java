package com.goole.androidexercise.paint;

import android.app.Activity;
import android.content.Context;
import android.graphics.AvoidXfermode;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.DiscretePathEffect;
import android.graphics.EmbossMaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SumPathEffect;
import android.graphics.Typeface;
import android.graphics.Xfermode;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.goole.androidexercise.R;

public class PaintDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.circle_image_layout);
        setContentView(new EraserView(this));
    }

    public class MyView extends View {

        private Paint mPaint;
        private int mCenterX;
        private int mCenterY;

        public MyView(Context context) {
            super(context);
            init();
        }

        public MyView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init();
        }

        private void init() {
            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaint.setColor(getResources().getColor(R.color.dark_blue));//画笔颜色
            mPaint.setStyle(Paint.Style.FILL);  //画笔风格
            mPaint.setTextSize(36);             //绘制文字大小，单位px
            mPaint.setStrokeWidth(5);           //画笔粗细
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            mCenterX = (int) (w / 2.0f);
            mCenterY = (int) (h / 2.0f);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            mPaint.setColor(getResources().getColor(R.color.dark_blue));
            canvas.drawColor(getResources().getColor(R.color.yellow));   //设置画布背景颜色
            canvas.drawCircle(mCenterX, mCenterY, 100, mPaint);           //画实心圆
            canvas.drawRect(10, 10, 200, 100, mPaint);            //画矩形

            canvas.drawRoundRect(new RectF(10,150,210,250),15,15,mPaint); //画圆角矩形

            Paint.FontMetrics fm = mPaint.getFontMetrics();
            // 获取文字高度
            float fFontHeight = (float) Math.ceil(fm.descent - fm.ascent);
            canvas.drawText("最喜欢看曹神日狗了~",10,250 + fFontHeight + 10,mPaint);    //绘制文字

            Path path = new Path();
            path.moveTo(50,50);
            path.lineTo(100, 100);
            path.lineTo(200, 200);
            path.lineTo(300, 300);
            path.close();
            canvas.drawTextOnPath("最喜欢看曹神日狗了~", path, 50, 50, mPaint);    //绘制文字
            mPaint.setColor(Color.RED);


        }
    }

    public class DrawView extends View{

        private Paint mPaint;  //绘制线条的Path
        private Path mPath;      //记录用户绘制的Path
        private Canvas mCanvas;  //内存中创建的Canvas
        private Bitmap mBitmap;  //缓存绘制的内容

        private int mLastX;
        private int mLastY;

        public DrawView(Context context) {
            super(context);
            init();
        }

        public DrawView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init();
        }

        private void init(){
            mPath = new Path();
            mPaint = new Paint();   //初始化画笔
            mPaint.setColor(Color.GREEN);
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND); //结合处为圆角
            mPaint.setStrokeCap(Paint.Cap.ROUND); // 设置转弯处为圆角
            mPaint.setStrokeWidth(20);   // 设置画笔宽度
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();
            // 初始化bitmap,Canvas
            mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        //重写该方法，在这里绘图
        @Override
        protected void onDraw(Canvas canvas) {
            drawPath();
            canvas.drawBitmap(mBitmap, 0, 0, null);
        }

        //绘制线条
        private void drawPath(){
            mCanvas.drawPath(mPath, mPaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {

            int action = event.getAction();
            int x = (int) event.getX();
            int y = (int) event.getY();

            switch (action)
            {
                case MotionEvent.ACTION_DOWN:
                    mLastX = x;
                    mLastY = y;
                    mPath.moveTo(mLastX, mLastY);
                    break;
                case MotionEvent.ACTION_MOVE:
                    int dx = Math.abs(x - mLastX);
                    int dy = Math.abs(y - mLastY);
                    if (dx > 3 || dy > 3)
                        mPath.lineTo(x, y);
                    mLastX = x;
                    mLastY = y;
                    break;
            }

            invalidate();
            return true;
        }
    }

    public class BlurMaskFilterView extends View {

        public BlurMaskFilterView(Context context) {
            super(context);
        }

        public BlurMaskFilterView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public BlurMaskFilterView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            BlurMaskFilter bmf = null;
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.RED);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextSize(68);
            paint.setStrokeWidth(5);

            //BlurMaskFilter.Blur.NORMAL：内外模糊
            //BlurMaskFilter.Blur.OUTER：外部模糊
            //BlurMaskFilter.Blur.INNER：内部模糊
            //BlurMaskFilter.Blur.SOLID：内部加粗，外部模糊

            bmf = new BlurMaskFilter(10f,BlurMaskFilter.Blur.NORMAL);
            paint.setMaskFilter(bmf);
            canvas.drawText("最喜欢看曹神日狗了~", 100, 100, paint);
            bmf = new BlurMaskFilter(10f,BlurMaskFilter.Blur.OUTER);
            paint.setMaskFilter(bmf);
            canvas.drawText("最喜欢看曹神日狗了~", 100, 200, paint);
            bmf = new BlurMaskFilter(10f,BlurMaskFilter.Blur.INNER);
            paint.setMaskFilter(bmf);
            canvas.drawText("最喜欢看曹神日狗了~", 100, 300, paint);
            bmf = new BlurMaskFilter(10f,BlurMaskFilter.Blur.SOLID);
            paint.setMaskFilter(bmf);
            canvas.drawText("最喜欢看曹神日狗了~", 100, 400, paint);

            bmf = new BlurMaskFilter(50f,BlurMaskFilter.Blur.NORMAL);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            paint.setMaskFilter(bmf);
            canvas.drawBitmap(bitmap, 0, 500, paint);

            bmf = new BlurMaskFilter(50f,BlurMaskFilter.Blur.NORMAL);
            paint.setMaskFilter(bmf);
            canvas.drawBitmap(bitmap, 200, 500, paint);

            bmf = new BlurMaskFilter(50f,BlurMaskFilter.Blur.INNER);
            paint.setMaskFilter(bmf);
            canvas.drawBitmap(bitmap, 400, 500, paint);

            bmf = new BlurMaskFilter(50f,BlurMaskFilter.Blur.SOLID);
            paint.setMaskFilter(bmf);
            canvas.drawBitmap(bitmap, 600, 500, paint);

            setLayerType(View.LAYER_TYPE_SOFTWARE, null);     //关闭硬件加速
        }
    }

    public class EmbossMaskFilterView extends View{

        public EmbossMaskFilterView(Context context) {
            super(context);
        }

        public EmbossMaskFilterView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public EmbossMaskFilterView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void onDraw(Canvas canvas) {

            canvas.drawColor(Color.BLACK);

            float[] direction = new float[]{ 1, 1, 3 };   // 设置光源的方向
            float light = 0.4f;     //设置环境光亮度
            float specular = 8;     // 定义镜面反射系数
            float blur = 3.0f;      //模糊半径
            EmbossMaskFilter emboss=new EmbossMaskFilter(direction,light,specular,blur);

            Paint paint = new Paint();
            paint.setAntiAlias(true);          //抗锯齿
            paint.setColor(Color.BLUE);//画笔颜色
            paint.setStyle(Paint.Style.FILL);  //画笔风格
            paint.setTextSize(70);             //绘制文字大小，单位px
            paint.setStrokeWidth(8);           //画笔粗细
            paint.setMaskFilter(emboss);

            paint.setMaskFilter(emboss);
            canvas.drawText("最喜欢看曹神日狗了~", 50, 100, paint);


            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.earth);
            Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            Rect dst = new Rect(200, 200, getWidth() - 200, getHeight() - 400);
            canvas.drawBitmap(bitmap, src, dst, paint);

            setLayerType(View.LAYER_TYPE_SOFTWARE, null);     //关闭硬件加速
        }
    }

    public class AvoidXfermodeView1 extends View {

        private Paint mPaint;
        private Bitmap mBitmap;
        private AvoidXfermode avoidXfermode;

        public AvoidXfermodeView1(Context context) {
            super(context);
            init();
        }

        public AvoidXfermodeView1(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public AvoidXfermodeView1(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init();
        }

        private void init() {
            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);  //抗锯齿
            avoidXfermode = new AvoidXfermode(0XFFCCD1D4, 0, AvoidXfermode.Mode.TARGET);
            mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_meizi);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawBitmap(mBitmap, 50, 50, mPaint);
            mPaint.setARGB(255, 222, 83, 71);
            mPaint.setXfermode(avoidXfermode);
            canvas.drawRect(50, 50, 690, 1010, mPaint);

            setLayerType(View.LAYER_TYPE_SOFTWARE, null);     //关闭硬件加速
        }
    }

    private static class XfermodeView extends View {
        private static final int W = 64 * 3;
        private static final int H = 64 * 3;
        private static final int ROW_MAX = 4;   // number of samples per row

        private Bitmap mSrcB;
        private Bitmap mDstB;
        private Shader mBG;     // background checker-board pattern

        private static final Xfermode[] sModes = {
                new PorterDuffXfermode(PorterDuff.Mode.CLEAR),
                new PorterDuffXfermode(PorterDuff.Mode.SRC),
                new PorterDuffXfermode(PorterDuff.Mode.DST),
                new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER),
                new PorterDuffXfermode(PorterDuff.Mode.DST_OVER),
                new PorterDuffXfermode(PorterDuff.Mode.SRC_IN),
                new PorterDuffXfermode(PorterDuff.Mode.DST_IN),
                new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT),
                new PorterDuffXfermode(PorterDuff.Mode.DST_OUT),
                new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP),
                new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP),
                new PorterDuffXfermode(PorterDuff.Mode.XOR),
                new PorterDuffXfermode(PorterDuff.Mode.DARKEN),
                new PorterDuffXfermode(PorterDuff.Mode.LIGHTEN),
                new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY),
                new PorterDuffXfermode(PorterDuff.Mode.SCREEN)
        };

        private static final String[] sLabels = {
                "Clear", "Src", "Dst", "SrcOver",
                "DstOver", "SrcIn", "DstIn", "SrcOut",
                "DstOut", "SrcATop", "DstATop", "Xor",
                "Darken", "Lighten", "Multiply", "Screen"
        };

        // create a bitmap with a circle, used for the "dst" image
        static Bitmap makeDst(int w, int h) {
            Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bm);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

            p.setColor(0xFFFFCC44);
            c.drawOval(new RectF(0, 0, w*3/4, h*3/4), p);
            return bm;
        }

        // create a bitmap with a rect, used for the "src" image
        static Bitmap makeSrc(int w, int h) {
            Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bm);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

            p.setColor(0xFF66AAFF);
            c.drawRect(w/3, h/3, w*19/20, h*19/20, p);
            return bm;
        }

        public XfermodeView(Context context) {
            super(context);

            mSrcB = makeSrc(W, H);
            mDstB = makeDst(W, H);

            // make a ckeckerboard pattern
            Bitmap bm = Bitmap.createBitmap(new int[] { 0xFFFFFFFF, 0xFFCCCCCC,
                            0xFFCCCCCC, 0xFFFFFFFF }, 2, 2,
                    Bitmap.Config.RGB_565);
            mBG = new BitmapShader(bm,
                    Shader.TileMode.REPEAT,
                    Shader.TileMode.REPEAT);
            Matrix m = new Matrix();
            m.setScale(6, 6);
            mBG.setLocalMatrix(m);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(Color.WHITE);

            Paint labelP = new Paint(Paint.ANTI_ALIAS_FLAG);
            labelP.setTextAlign(Paint.Align.CENTER);
            labelP.setTextSize(45);
            labelP.setTypeface(Typeface.DEFAULT_BOLD);

            Paint paint = new Paint();
            paint.setFilterBitmap(false);

            canvas.translate(15, 35);

            int x = 0;
            int y = 100;
            for (int i = 0; i < sModes.length; i++) {
                // draw the border
                paint.setStyle(Paint.Style.STROKE);
                paint.setShader(null);
                canvas.drawRect(x - 0.5f, y - 0.5f,
                        x + W + 0.5f, y + H + 0.5f, paint);

                // draw the checker-board pattern
                paint.setStyle(Paint.Style.FILL);
                paint.setShader(mBG);
                canvas.drawRect(x, y, x + W, y + H, paint);

                //创建一个图层，在图层上演示图形混合后的效果
                // draw the src/dst example into our offscreen bitmap
                int sc = canvas.saveLayer(x, y, x + W, y + H, null,
                        Canvas.MATRIX_SAVE_FLAG |
                                Canvas.CLIP_SAVE_FLAG |
                                Canvas.HAS_ALPHA_LAYER_SAVE_FLAG |
                                Canvas.FULL_COLOR_LAYER_SAVE_FLAG |
                                Canvas.CLIP_TO_LAYER_SAVE_FLAG);
                canvas.translate(x, y);
                canvas.drawBitmap(mDstB, 0, 0, paint);
                paint.setXfermode(sModes[i]);
                canvas.drawBitmap(mSrcB, 0, 0, paint);
                paint.setXfermode(null);
                canvas.restoreToCount(sc);

                // draw the label
                canvas.drawText(sLabels[i],
                        x + W/2, y - labelP.getTextSize()/2, labelP);

                x += W + 30;

                // wrap around when we've drawn enough for one row
                if ((i % ROW_MAX) == ROW_MAX - 1) {
                    x = 0;
                    y += H + 100;
                }
            }
        }
    }

    public class PathEffectView extends View {

        private Paint mPaint;
        private Path mPath;
        private float phase = 0;
        private PathEffect[] effects = new PathEffect[7];
        private int[] colors;

        public PathEffectView(Context context) {
            this(context, null);
        }

        public PathEffectView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public PathEffectView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        //初始化画笔
        private void init() {
            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG); //抗锯齿
            mPaint.setStyle(Paint.Style.STROKE);       //绘画风格:空心
            mPaint.setStrokeWidth(5);                  //笔触粗细
            mPath = new Path();
            mPath.moveTo(0, 0);
            for (int i = 1; i <= 30; i++) {
                // 生成30个点，随机生成它们的坐标，并将它们连成一条Path
                mPath.lineTo(i * 40, (float) Math.random() * 100);
            }
            // 初始化7个颜色
            colors = new int[] { Color.RED, Color.BLUE, Color.GREEN,
                    Color.YELLOW, Color.BLACK, Color.MAGENTA, Color.CYAN };
        }


        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(Color.WHITE);
            //初始化其中路径效果：
            effects[0] = null;                                    //无效果
            effects[1] = new CornerPathEffect(10);                //CornerPathEffect
            effects[2] = new DiscretePathEffect(3.0f, 5.0f);      //DiscretePathEffect
            effects[3] = new DashPathEffect(new float[] { 20, 10, 5, 10 },phase);   //DashPathEffect
            Path p = new Path();
            p.addRect(0, 0, 8, 8, Path.Direction.CCW);
            effects[4] = new PathDashPathEffect(p, 12, phase,
                    PathDashPathEffect.Style.ROTATE);             //PathDashPathEffect
            effects[5] = new ComposePathEffect(effects[2], effects[4]);    //ComposePathEffect
            effects[6] = new SumPathEffect(effects[2], effects[4]);   //SumPathEffect
            // 将画布移动到(10,10)处开始绘制
            canvas.translate(10, 10);
            // 依次使用7中不同的路径效果、7中不同的颜色来绘制路径
            for (int i = 0; i < effects.length; i++) {
                mPaint.setPathEffect(effects[i]);
                mPaint.setColor(colors[i]);
                canvas.drawPath(mPath, mPaint);
                canvas.translate(0, 120);
            }
            // 改变phase值，形成动画效果
            phase += 2;
            invalidate();
        }
    }

}
