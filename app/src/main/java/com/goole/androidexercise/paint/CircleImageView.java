package com.goole.androidexercise.paint;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.goole.androidexercise.R;

public class CircleImageView extends View {

    private Bitmap mSrc;

    //图片相关的属性
    private int type;                           //类型，圆形或者圆角
    public static final int TYPE_CIRCLE = 0;
    public static final int TYPE_ROUND = 1;
    private static final int BODER_RADIUS_DEFAULT = 10;     //圆角默认大小值
    private int mBorderRadius;                  //圆角大小
    private boolean mFitImage;                  // If need to adjust the size according the drawable
    private int mBgColor;                           // background color, It works only when mFitImage is false and the size of the view is bigger than drawable size

    public CircleImageView(Context context) {
        this(context, null);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, defStyleAttr, 0);
        mBorderRadius = ta.getDimensionPixelSize(R.styleable.CircleImageView_Radius, BODER_RADIUS_DEFAULT);
        type = ta.getInt(R.styleable.CircleImageView_type, 0);
        mFitImage = ta.getBoolean(R.styleable.CircleImageView_fit_image, true);
        mSrc = BitmapFactory.decodeResource(getResources(), ta.getResourceId(R.styleable.CircleImageView_src, 0));
        mBgColor = ta.getColor(R.styleable.CircleImageView_bg_color, Color.CYAN);
        ta.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (type == TYPE_CIRCLE) {
            int width = Math.min(getMeasuredWidth(), getMeasuredHeight());
            int srcWidth = Math.min(mSrc.getWidth(), mSrc.getHeight());
            if(mFitImage) {
                width = Math.min(width, srcWidth);
            }
            setMeasuredDimension(width, width);    //设置当前View的大小
        }

        if (type == TYPE_ROUND) {
            int width = Math.min(mSrc.getWidth(), getMeasuredWidth());
            int height = Math.min(mSrc.getHeight(), getMeasuredHeight());
            setMeasuredDimension(width, height);
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(createDstBitmap(mSrc), 0, 0, null);
    }


    private Bitmap createDstBitmap(Bitmap source) {
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(mBgColor);
        if (type == TYPE_ROUND) {
            canvas.drawRoundRect(new RectF(0, 0, getWidth(), getHeight()),
                    mBorderRadius, mBorderRadius, paint);
        } else {
            canvas.drawCircle(getWidth() / 2, getWidth() / 2, getWidth() / 2, paint);
        }

        int left;
        int top;

        if(mSrc.getWidth() > getWidth()) {
            left = (int) (-(mSrc.getWidth() - getWidth()) / 2.0f);
        } else {
            left = (int) ((getWidth() - mSrc.getWidth()) / 2.0f);
        }

        if (mSrc.getHeight() > getHeight()) {
            top = (int) (-(mSrc.getHeight() - getHeight()) / 2.0f);
        } else {
            top = (int) ((getHeight() - mSrc.getHeight()) / 2.0f);
        }

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, left, top, paint);

        return  bitmap;
    }
}
