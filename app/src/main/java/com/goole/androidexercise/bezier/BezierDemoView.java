package com.goole.androidexercise.bezier;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class BezierDemoView extends View {

    public final static String TAG = BezierDemoView.class.getSimpleName();

    private Paint mPaint;

    private PointF mDragCenter;
    private PointF mStickCenter;

    private int mDragRadius;
    private int mStickRadius;

    private int mDragBoundRadius;

    private PointF[] mDragPoints = {new PointF(), new PointF()};
    private PointF[] mStickPoints = {new PointF(), new PointF()};

    private float mDegree;

    public BezierDemoView(Context context) {
        super(context);
        init();
    }

    public BezierDemoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BezierDemoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.RED);
        mDragBoundRadius = 200;
        mStickRadius = 20;
        mDragRadius = 20;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mStickCenter = new PointF(w / 2, h / 2);
        mDragCenter = new PointF(w / 2, h / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(mStickCenter.x, mStickCenter.y, mDragBoundRadius, mPaint);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(mStickCenter.x, mStickCenter.y, mStickRadius, mPaint);
        canvas.drawCircle(mDragCenter.x, mDragCenter.y, mDragRadius, mPaint);

        mPaint.setColor(Color.BLUE);
        Path path = new Path();
        path.moveTo(mDragPoints[0].x, mDragPoints[0].y);
        path.quadTo(mDragPoints[0].x, mDragPoints[0].y, mStickPoints[0].x, mStickPoints[0].y);
        path.lineTo(mStickPoints[1].x, mStickPoints[1].y);
        path.quadTo(mStickPoints[1].x, mStickPoints[1].y, mDragPoints[1].x, mDragPoints[1].y);
        canvas.drawPath(path, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getRawX();
        float y = event.getRawY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                mDragCenter.set(x, y);
                double distance = Math.sqrt(Math.pow((mDragCenter.x - mStickCenter.x), 2) + Math.pow((mDragCenter.y - mStickCenter.y), 2));
                Log.d(TAG, "distance = " + distance);
                mDegree = (float) Math.atan(mStickRadius / distance);
                Log.d(TAG, "degree = " + 180 * mDegree);
                float dragDltaX = (float) (mDragRadius * Math.sin(mDegree));
                float dragDltaY = (float) (mDragRadius * Math.cos(mDegree));
                Log.d(TAG, " dltaX = " + dragDltaX + "   dltaY = " + dragDltaY);
                mDragPoints[0].set(mDragCenter.x - dragDltaX, mDragCenter.y - dragDltaY);
                mDragPoints[1].set(mDragCenter.x + dragDltaX, mDragCenter.y + dragDltaY);

                Log.d(TAG, "point.x = " + mDragCenter.x + "   point.y = " + mDragCenter.y);
                Log.d(TAG, "point0.x = " + mDragPoints[0].x + "   point0.y = " + mDragPoints[0].y);

                mStickPoints[0].set(mStickCenter.x - dragDltaX, mStickCenter.y - dragDltaY);
                mStickPoints[1].set(mStickCenter.x + dragDltaX, mStickCenter.y + dragDltaY);
                break;
            case MotionEvent.ACTION_UP:

                break;
        }
        invalidate();
        return true;
    }
}
