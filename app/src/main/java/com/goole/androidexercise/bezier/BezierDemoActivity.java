package com.goole.androidexercise.bezier;

import android.app.Activity;
import android.os.Bundle;

import com.goole.androidexercise.R;

public class BezierDemoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bezier_layout);
    }
}
