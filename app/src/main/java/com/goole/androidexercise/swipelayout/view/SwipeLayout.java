package com.goole.androidexercise.swipelayout.view;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class SwipeLayout extends FrameLayout {

    private final static String TAG = "SwipeLayout";

    private ViewDragHelper mDragger;
    private View mDragView;
    private View mButton;

    private OnStateListener mStateListener;

    private int mDragDistance = 0;

    private Point mOriginPos = new Point();

    public SwipeLayout(Context context) {
        this(context, null, 0);
    }

    public SwipeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public interface OnStateListener {
        void onOpen(View view);
        void onDownOrMove(View view);
    }

    public void setOnStateListener(OnStateListener listener) {
        mStateListener = listener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public SwipeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mDragger = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child == mDragView;
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx)
            {
                return Math.min(Math.max(-mButton.getWidth() + getPaddingLeft(), left), getPaddingLeft());
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy)
            {
                return getPaddingTop();
            }

            @Override
            public int getViewHorizontalDragRange(View child) {
                return 1;
            }

            @Override
            public int getViewVerticalDragRange(View child)
            {
                return getMeasuredHeight()-child.getMeasuredHeight();
            }

            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                super.onViewReleased(releasedChild, xvel, yvel);
                mDragDistance = Math.abs(releasedChild.getLeft());
                Log.d(TAG, "mDragDistance = " + mDragDistance + "  mDragView Width = " + mButton.getWidth());
                if (mDragDistance < mButton.getWidth() / 2)
                {
                    mDragger.settleCapturedViewAt(mOriginPos.x, mOriginPos.y);
                    ViewCompat.postInvalidateOnAnimation(SwipeLayout.this);
                } else {
                    mDragger.settleCapturedViewAt(-mButton.getWidth() + getPaddingLeft(), mOriginPos.y);
                    ViewCompat.postInvalidateOnAnimation(SwipeLayout.this);
                    if(mStateListener != null) mStateListener.onOpen(SwipeLayout.this);
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            if(mStateListener != null) mStateListener.onDownOrMove(this);
        }
        return mDragger.shouldInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        mDragger.processTouchEvent(event);
        return true;
    }

    @Override
    public void computeScroll()
    {
        if(mDragger.continueSettling(true))
        {
            ViewCompat.postInvalidateOnAnimation(SwipeLayout.this);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        mOriginPos.x = mDragView.getLeft();
        mOriginPos.y = mDragView.getTop();
    }

    public void open() {
        mDragger.smoothSlideViewTo(mDragView, -mDragDistance + getPaddingLeft(), getPaddingTop());
        ViewCompat.postInvalidateOnAnimation(SwipeLayout.this);
        if(mStateListener != null) mStateListener.onOpen(SwipeLayout.this);
    }

    public void close() {
        mDragger.smoothSlideViewTo(mDragView, getPaddingLeft(), getPaddingTop());
        ViewCompat.postInvalidateOnAnimation(SwipeLayout.this);
    }

    public boolean isOpen() {
        return mDragger.isViewUnder(mDragView, -mButton.getWidth(), getPaddingTop());
    }


    @Override
    public void setOnClickListener(OnClickListener l) {
        mDragView.setOnClickListener(l);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(getChildCount() > 2) {
            throw new IllegalArgumentException("SwipeLayout only has tow child view");
        }
        mButton = getChildAt(0);
        mDragView = getChildAt(1);
        if(mButton instanceof ViewGroup) {
            if(!(mButton instanceof LinearLayout)) {
                throw new IllegalArgumentException("the hidden view should be a vew or a LinearLayout");
            } else {
                int orientation = ((LinearLayout) mButton).getOrientation();
                if(orientation == LinearLayout.VERTICAL && ((LinearLayout) mButton).getChildCount() > 1) {
                    throw new IllegalArgumentException("The orientation of LinearLayout now only support LinearLayout.HORIZONTAL way");
                }
            }
        }
        // The elevation of mDragView should be bigger than mButton if the api level >= 21 and mButton is a Button
        if(Build.VERSION.SDK_INT >= 21 && mButton instanceof Button && mDragView.getElevation() <= mButton.getElevation()) {
            mDragView.setElevation(mButton.getElevation() + 10);
        }
    }
}
